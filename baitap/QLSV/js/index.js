var dssv = [];
// lấy data JSON
var dataJSON = localStorage.getItem("DSSV");
// chuyển đổi JSON thành JS
if (dataJSON) {
   var dataRaw = JSON.parse(dataJSON);
   dssv = dataRaw.map(function (item) {
    return new sinhVien(
      item.ma,
      item.ten,
      item.email,
      item.matKhau,
      item.diemToan,
      item.diemLy,
      item.diemHoa
    );
  });
  renderDSSV(dssv);
}
// tạo funtion lưu datta local
function saveLocal() {
  // Tạo localStorge ~ Convert dữ liệu sang JSON
  var dssvJSON = JSON.stringify(dssv);
  localStorage.setItem("DSSV", dssvJSON);
}

function addSV() {
  var newSV = informationForm();
  // var isValid=true
  // check mã sinh viên
  // isValid &= checkEmpty(newSV.ma,"spanMaSV" )&& checkIdSV(newSV.ma,dssv,"spanMaSV")
  // kiểm tra tên
  // isValid &= checkEmpty(newSV.ten,"spanTenSV" )
  // check định dạng email
  // isValid &=checkEmpty(newSV.email,"spanEmailSV")&& checkEmail(newSV.email,"spanEmailSV") 
  // check mật khẩu
  // isValid &= checkEmpty(newSV.matKhau,"spanMatKhau")
 
  dssv.push(newSV);
    saveLocal();
    renderDSSV(dssv);
    resetForm();
  // if (isValid) {
  //   dssv.push(newSV);
  //   saveLocal();
  //   renderDSSV(dssv);
  //   resetForm();
  // }
 

}

function deleteSV(idSV) {
  var index = dssv.findIndex(function (SV) {
    return SV.ma == idSV;
  });
  if (index == -1) {
    return;
  }
  dssv.splice(index, 1);
  saveLocal();
  renderDSSV(dssv);
}

function editSV(idSV) {
  var index= dssv.findIndex(function(SV){
    return SV.ma == idSV
  })
  if (index == -1){
    return
  }
  var SV= dssv[index]
  showInformation(SV)
  document.getElementById('txtMaSV').disabled = true
}

function updateInformation(){
var svEdit= informationForm()
var index= dssv.findIndex(function(SV){
  return SV.ma == svEdit.ma
})
if (index == -1){
  return  
}
dssv[index]=svEdit
saveLocal();
renderDSSV(dssv)
resetForm()

}