
const BASE_URL = "https://633ec0600dbc3309f3bc551f.mockapi.io";
var dssv=[]

var batLoading=function(){
    document.getElementById("loading").style.display="flex"
}
var tatLoading=function(){
    document.getElementById("loading").style.display="none"
}


var fletchDSSV=function(){
    batLoading()
    axios({
        url:`${BASE_URL}/QLSV`,
        method: "GET",
      })
        .then(function (res) {
          renderDSSV(res.data)
          dssv=res.data
          tatLoading()
        })
        .catch(function (err) {
            tatLoading()
        });
}
fletchDSSV()

  var renderDSSV=function(list){
    var contentHTML=""
    list.forEach(function(SV){
        contentHTML+=`<tr>
        <td>${SV.ma}</td>
        <td>${SV.ten}</td>
        <td>${SV.email}</td>
        <td>0</td>
        <td>
        <button onclick="suaSV(${SV.ma})" class="btn btn-primary" >Sửa</button>
        <button onclick="xoaSV(${SV.ma})" class="btn btn-danger" >Xóa</button>
        </td>
        
        
        </tr>`
    })
    document.getElementById("tbodySinhVien").innerHTML=contentHTML
  }
  var xoaSV=function(idSV){
    axios({
        url: `${BASE_URL}/QLSV/${idSV}`,
        method: "DELETE"
    })
    .then(function(res){
       fletchDSSV()
       Swal.fire('Xóa thành công')

    })
    .catch(function(err){
        Swal.fire('Xóa không thành công')

    })
  }
  var addSV=function(){
    var SV=informationForm();
   axios({
    url: `${BASE_URL}/QLSV`,
    method: "POST",
    data:SV
   })
   .then(function(res){
    fletchDSSV()
    Swal.fire('Thêm thành công')
   })
   .catch(function(err){
    Swal.fire('Thêm không thành công')

   })

  }

  var suaSV=function(idSV){
    axios
    .put(`${BASE_URL}/QLSV/${idSV}`)
    .then(function(res){
      var index= dssv.findIndex(function(SV){
        return SV.ma == idSV
      })
      if (index == -1){
        return
      }
      var SV= dssv[index]
      showInformation(SV)
      document.getElementById('txtMaSV').disabled = true
    })
    .catch(function(err){

    })
  }
  var updateInformation=function(){
    var SV=informationForm()
    axios({
      url:`${BASE_URL}/QLSV/${SV.ma}`,
      method:"PUT",
      data:SV
    })
    .then(function(res){
      Swal.fire('Cập nhật thành công')
    })
    .catch(function(err){
      Swal.fire('Cập nhật Không thành công')
    })
  }